// 1,0,0,0  --> Lone Wolf
// 0,1,0,0  --> Busy Bee
// 0,0,1,0  --> Chill Sloth
// 0,0,0,1  --> Cuddly Bear

/////////////// ENGLISH VERSION ///////////////
export const QuestionsEn = [
  //Question 1
  {
    question:"What kind of interaction you'd like to have with your possible flatmate?",
    replies:[
      { key:1, points:[0,0,0,1], content:"Besties!!!" },
      { key:2, points:[0,1,1,0], content:"I'd like to get to know each other" },
      { key:3, points:[1,0,0,0], content:"I'd prefer our interaction to be minimal/formal to avoid crossing any lines" },
    ],
    type:'regular'
  },
  //Question 2
  {
    question:"Which room of the house you usually spend most of your time in?",
    replies:[
      { key:1, points:[0,0,0,0], content:"Bedroom" },
      { key:2, points:[0,0,0,0], content:"Kitchen" },
      { key:3, points:[0,0,0,0], content:"Living Room" },
    ],
    type:'regular'
  },
  //Question 3
  {
    question:"What time does your alarm clock usually set off?",
    replies:[
      { key:1, points:[1,0,0,0], content:"Before 6am" },
      { key:2, points:[0,1,0,0], content:"Some time between 7am and 9am" },
      { key:3, points:[0,0,0,1], content:"Past the 10am" },
      { key:4, points:[0,0,1,0], content:"I don't use an alarm clock" },
    ],
    type:'regular'
  },
  //Question 4
  {
    question:"What time do you usually go to bed?",
    replies:[
      { key:1, points:[1,0,0,0], content:"Bewteen 22:00 - 00:00" },
      { key:2, points:[0,0,1,0], content:"Just before dawn" },
      { key:3, points:[0,1,0,1], content:"It depends" },
    ],
    type:'regular'
  },
  //Question 5
  {
    question:"How often do you clean the bathroom?",
    replies:[
      { key:1, points:[1,0,0,0], content:"Everyday" },
      { key:2, points:[0,1,0,0], content:"Once every week" },
      { key:3, points:[0,0,0,1], content:"Once every month" },
      { key:4, points:[0,0,1,0], content:"I don't" },
    ],
    type:'regular'
  },
  //Question 6
  {
    question:"What do you usually do on a Friday evening?",
    replies:[
      { key:1, points:[0,0,0,1], content:"Clubbing", img:"./assets/gifs/gif1.gif" },
      { key:2, points:[0,0,0,1], content:"Hanging out with friends", img:"./assets/gifs/gif2.gif" },
      { key:3, points:[0,1,0,0], content:"Pumperig myself", img:"./assets/gifs/gif3.gif" },
      { key:4, points:[1,0,1,0], content:"Quality time with my PC", img:"./assets/gifs/gif4.gif" },
    ],
    type:'picture'
  },
  //Question 7
  {
    question:"You stumble upon a tray full of your flatmate's forgotten cake in the kitchen, what do you do?",
    replies:[
      { key:1, points:[0,0,0,1], content:"You put it in the refrigerator" },
      { key:2, points:[0,0,1,0], content:"You have a slice" },
      { key:3, points:[0,1,0,0], content:"You ask him/her whether you could have a slice" },
      { key:4, points:[1,0,0,0], content:"You order/cook one for youself" },
    ],
    type:'regular'
  },
  //Question 8
  {
    question:"You come to the realization that your flatmate has borrowed something that's yours without permission. How would you handle it?",
    replies:[
      { key:1, points:[1,0,0,0], content:"You get mad with him/her and you permit them from entering your room ever again." },
      { key:2, points:[0,1,0,0], content:"\"I just point out to him/her the fact that I'd like know before they ever do that again.\"" },
      { key:3, points:[0,0,1,1], content:"\"I don't mind... I've borrowed their stuff without them knowing before\"" },
    ],
    type:'regular'
  },
  //Question 9
  {
    question:"Your bedroom usually is...",
    replies:[
      { key:1, points:[0,1,0,1], content:"Tidied up" },
      { key:2, points:[1,0,0,0], content:"\"Sterilized\"" },
      { key:3, points:[0,0,1,0], content:"Messy" },
    ],
    type:'regular'
  },
  //Question 10
  {
    question:"What are your feelings towards household chores?",
    replies:[
      { key:1, points:[0,1,0,0], content:"\"They're not really my thing, but they have to be dealt with.\"" },
      { key:2, points:[0,0,0,1], content:"\"If it's a shared effort, I'm okay with them.\"" },
      { key:3, points:[1,0,0,0], content:"\"I love cleaning and stuff.\"" },
      { key:4, points:[0,0,1,0], content:"\"I hate them!\"" },
    ],
    type:'regular'
  },
  //Question 11
  {
    question:"What's one characteristic that you'd never want your flatmate to have?",
    replies:[
      { key:1, points:[0,0,0,0], content:"Noisy" },
      { key:2, points:[0,0,0,0], content:"Disorderly" },
      { key:3, points:[0,0,0,0], content:"Obsessive with cleanliness" },
      { key:4, points:[0,0,0,0], content:"Babbler" },
      { key:5, points:[0,0,0,0], content:"Invasive" },
    ],
    type:'regular'
  },
  //Question 12
  {
    question:"I don't really care about my flatmate's sexual orientation",
    subtitle:"(1 to 5, how much do you agree?)",
    replies:[
      { key:1, points:[0,0,0,0], content:"" },
      { key:2, points:[0,0,0,0], content:"" },
      { key:3, points:[0,0,0,0], content:"" },
      { key:4, points:[0,0,0,0], content:"" },
      { key:5, points:[0,0,0,0], content:"" },
    ],
    type:'enum'
  },
  //Question 13
  {
    question:"When and how do you usually pay/would pay your rent?",
    replies:[
      { key:1, points:[0,1,0,0], content:"Through the Internet a few days before the due date" },
      { key:2, points:[1,0,0,0], content:"\"I've got the payment automated with an e-banking app\"" },
      { key:3, points:[0,0,0,1], content:"\"My parents pay for it\"" },
      { key:4, points:[0,0,1,0], content:"\"I deliver it in cash form to the landlord\"" },
    ],
    type:'regular'
  },
  //Question 14
  {
    question:"Which of the following you'd never forgive your flatmate for if it ever came to it?",
    replies:[
      { key:1, points:[0,1,0,0], content:"Having them eat the last piece of a cake you'd saved up for yourself in the fridge" },
      { key:2, points:[1,0,0,0], content:"Having them blasting out music when you're sleeping" },
      { key:3, points:[0,0,0,1], content:"Having them forget your birthday" },
      { key:4, points:[0,0,1,0], content:"Having them clean up my bedroom" },
    ],
    type:'regular'
  },
  //Question 15
  {
    question:"Which of the following characterizations sounds like you the most?",
    replies:[
      { key:1, points:[0,0,0,1], content:"Sociable" },
      { key:2, points:[0,0,1,0], content:"Easy-going" },
      { key:3, points:[1,0,0,0], content:"Responsible" },
      { key:4, points:[0,1,0,0], content:"Discreet" },
    ],
    type:'regular'
  },
  //Question 16
  {
    question:"If somebody could blame you for one thing, that would be that:",
    replies:[
      { key:1, points:[0,0,0,0], content:"\"I'm a bit Noisy at times\"" },
      { key:2, points:[0,0,0,0], content:"\"I'm a somewhat Disorderly\"" },
      { key:3, points:[0,0,0,0], content:"\"I'm Obsessive with Cleanliness\"" },
      { key:4, points:[0,0,0,0], content:"\"I'm a bit of a Babbler at times\"" },
      { key:5, points:[0,0,0,0], content:"\"I'm a little Invasive at times\"" },
    ],
    type:'regular'
  },
  //Question 17
  {
    question:"If I had a choice, I'd live in a house by myself only",
    subtitle:"(1 to 5, how much do you agree?)",
    replies:[
      { key:1, points:[1,0,0,0], content:"" },
      { key:2, points:[0,1,0,0], content:"" },
      { key:3, points:[0,0,1,1], content:"" },
      { key:4, points:[0,0,1,1], content:"" },
      { key:5, points:[0,0,1,1], content:"" },
    ],
    type:'enum'
  },
  //Question 18
  {
    question:"Which of the following sounds like the job of your dreams?",
    replies:[
      { key:1, points:[0,0,1,0], content:"Artist" },
      { key:2, points:[1,0,0,0], content:"Business Executive" },
      { key:3, points:[0,1,0,0], content:"Freelancer" },
      { key:4, points:[0,0,0,1], content:"Teacher" },
      { key:5, points:[1,0,0,0], content:"Medic" },
      { key:6, points:[1,0,0,0], content:"Lawyer" },
    ],
    type:'regular'
  },
];

/////////////// GREEK VERSION ///////////////
export const QuestionsGr = [
  //Question 1
  {
    question:"Διάλεξε τι είδους σχέση επιθυμείς να έχεις με τον/την συγκάτοικό σου:",
    replies:[
      { key:1, points:[0,0,0,1], content:"Κολλητοί!!!" },
      { key:2, points:[0,0,1,0], content:"Θα μου άρεσε να γνωριστούμε καλύτερα" },
      { key:3, points:[1,0,0,0], content:"Θα προτιμούσα οι σχέσεις μας να είναι τυπικές ώστε να διατηρούνται τα όρια" },
    ],
    type:'regular'
  },
  //Question 2
  {
    question:"Σε ποιο δωμάτιο του σπιτιού αφιερώνεις περισσότερο χρόνο;",
    replies:[
      { key:1, points:[0,0,0,0], content:"Υπνοδωμάτιο" },
      { key:2, points:[0,0,0,0], content:"Κουζίνα" },
      { key:3, points:[0,0,0,0], content:"Σαλόνι" },
    ],
    type:'regular'
  },
  //Question 3
  {
    question:"Τι ώρα χτυπάει το ξυπνητήρι σου το πρωί;",
    replies:[
      { key:1, points:[1,0,0,0], content:"Πριν τις 6:00 π.μ." },
      { key:2, points:[0,1,0,0], content:"Από τις 7:00 π.μ. - 9:00 π.μ." },
      { key:3, points:[0,0,0,1], content:"Μετά τις 10:00 π.μ." },
      { key:4, points:[0,0,1,0], content:"Δεν βάζω ξυπνητήρι" },
    ],
    type:'regular'
  },
  //Question 4
  {
    question:"Τι ώρα πας συνήθως για ύπνο;",
    replies:[
      { key:1, points:[1,0,0,0], content:"Μεταξύ 22:00-00:00" },
      { key:2, points:[0,0,1,0], content:"Το πρωί" },
      { key:3, points:[0,1,0,1], content:"Εξαρτάται" },
    ],
    type:'regular'
  },
  //Question 5
  {
    question:"Πόσο συχνά καθαρίζεις το μπάνιο;",
    replies:[
      { key:1, points:[1,0,0,0], content:"Κάθε μέρα" },
      { key:2, points:[0,1,0,0], content:"Μία φορά την εβδομάδα" },
      { key:3, points:[0,0,0,1], content:"Μια φορά το μήνα" },
      { key:4, points:[0,0,1,0], content:"Ποτέ" },
    ],
    type:'regular'
  },
  //Question 6
  {
    question:"Τι κάνεις το βράδυ της Παρασκευής;",
    replies:[
      { key:1, points:[0,0,0,1], content:"Κλαμπάρω", img:"./assets/gifs/gif1.gif" },
      { key:2, points:[0,0,0,1], content:"Την περνάω με φίλους", img:"./assets/gifs/gif2.gif" },
      { key:3, points:[0,1,0,0], content:"Καλοπίζομαι", img:"./assets/gifs/gif3.gif" },
      { key:4, points:[1,0,1,0], content:"Αράζω στο PC", img:"./assets/gifs/gif4.gif" },
    ],
    type:'picture'
  },
  //Question 7
  {
    question:"Βρίσκεις ένα ταψί σπανακόπιτα στη κουζίνα, τι κάνεις;",
    replies:[
      { key:1, points:[0,0,0,1], content:"Τη βάζεις στο ψυγείο" },
      { key:2, points:[0,0,1,0], content:"Τσιμπάς ένα κομμάτι" },
      { key:3, points:[0,1,0,0], content:"Ρωτάς τον/την συγκάτοικό σου αν μπορείς να έχεις ένα κομμάτι" },
      { key:4, points:[1,0,0,0], content:"Παραγγέλνεις μία για σένα" },
    ],
    type:'regular'
  },
  //Question 8
  {
    question:"Συνειδητοποείς ότι ο/η συγκάτοικος σου δανείστηκε κάτι δικό σου χωρίς άδεια. Πως το διαχειρίζεσαι;",
    replies:[
      { key:1, points:[1,0,0,0], content:"Βάζεις τις φωνές και του απαγορεύεις να ξαναμπεί στο δωμάτιο σου" },
      { key:2, points:[0,1,0,0], content:"Απλά του επισημαίνω ότι θα ήθελα να το γνωρίζω νωρίτερα" },
      { key:3, points:[0,0,1,1], content:"Δεν με πειράζει...δανείστηκα κι εγώ τη μπλούζα του κρυφά ούτως ή άλλως" },
    ],
    type:'regular'
  },
  //Question 9
  {
    question:"Το δωμάτιο μου είναι συνήθως",
    replies:[
      { key:1, points:[0,1,0,1], content:"Τακτοποιημένο" },
      { key:2, points:[1,0,0,0], content:"Αποστειρωμένο" },
      { key:3, points:[0,0,1,0], content:"Αμάζευτο" },
    ],
    type:'regular'
  },
  //Question 10
  {
    question:"Πως αισθάνεσαι για τις δουλειές του σπιτιού;",
    replies:[
      { key:1, points:[0,1,0,0], content:"Δεν τρελαίνομαι, αλλά πρέπει να γίνουν." },
      { key:2, points:[0,0,0,1], content:"Αν το κάνουμε όλοι μαζί, οκ." },
      { key:3, points:[1,0,0,0], content:"Λατρεύω το καθάρισμα!" },
      { key:4, points:[0,0,1,0], content:"Τις μισώ!" },
    ],
    type:'regular'
  },
  //Question 11
  {
    question:"Ποιο χαρακτηριστικό δεν θα ήθελες να έχει με τίποτα ο συγκάτοικός σου;",
    replies:[
      { key:1, points:[0,0,0,0], content:"Φασαριόζος" },
      { key:2, points:[0,0,0,0], content:"Ακατάστατος" },
      { key:3, points:[0,0,0,0], content:"Μανιακός με την καθαριότητα" },
      { key:4, points:[0,0,0,0], content:"Πολυλογάς" },
      { key:5, points:[0,0,0,0], content:"Αδιάκριτος" },
    ],
    type:'regular'
  },
  //Question 12
  {
    question:"Δεν έχω κάποια ιδιαίτερη προτίμηση για τις σεξουαλικές επιλογές του συγκατοίκου μου",
    subtitle:"(από το 1 έως το 5 πόσο συμφωνείς;)",
    replies:[
      { key:1, points:[0,0,0,0], content:"" },
      { key:2, points:[0,0,0,0], content:"" },
      { key:3, points:[0,0,0,0], content:"" },
      { key:4, points:[0,0,0,0], content:"" },
      { key:5, points:[0,0,0,0], content:"" },
    ],
    type:'enum'
  },
  //Question 13
  {
    question:"Πότε και πως πληρώνεις/θα πλήρωνες το ενοίκιο;",
    replies:[
      { key:1, points:[0,1,0,0], content:"Διαδικτυακά λίγες μέρες πριν τη προθεσμία" },
      { key:2, points:[1,0,0,0], content:"Έχω αυτοματοποιημένη την πληρωμή στο e-banking" },
      { key:3, points:[0,0,0,1], content:"Το πληρώνουν οι γονείς μου" },
      { key:4, points:[0,0,1,0], content:"Με μετρητά στον σπιτονοικοκύρη" },
    ],
    type:'regular'
  },
  //Question 14
  {
    question:"Τι δεν θα συγχωρούσες στον συγκάτοικό σου;",
    replies:[
      { key:1, points:[0,1,0,0], content:"Να φάει το φυλαγμένο γλυκό που είχα αφήσει στο ψυγείο" },
      { key:2, points:[1,0,0,0], content:"Να παίζει δυνατά μουσική όταν κοιμάμαι" },
      { key:3, points:[0,0,0,1], content:"Να ξεχάσει τα γενέθλιά μου" },
      { key:4, points:[0,0,1,0], content:"Να συμμαζέψει το δωμάτιο μου" },
    ],
    type:'regular'
  },
  //Question 15
  {
    question:"Πως θα περιέγραφες τον εαυτό σου;",
    replies:[
      { key:1, points:[0,0,0,1], content:"Κοινωνικός" },
      { key:2, points:[0,0,1,0], content:"Easy-going" },
      { key:3, points:[1,0,0,0], content:"Υπεύθυνος" },
      { key:4, points:[0,1,0,0], content:"Διακριτικός" },
    ],
    type:'regular'
  },
  //Question 16
  {
    question:"Αν θα μπορούσαν να σε κατηγορήσουν για κάτι, αυτό θα ήταν",
    replies:[
      { key:1, points:[0,0,0,0], content:"Φασαριόζος" },
      { key:2, points:[0,0,0,0], content:"Ακατάστατος" },
      { key:3, points:[0,0,0,0], content:"Μανιακός με την καθαριότητα" },
      { key:4, points:[0,0,0,0], content:"Πολυλογάς" },
      { key:5, points:[0,0,0,0], content:"Αδιάκριτος" },
    ],
    type:'regular'
  },
  //Question 17
  {
    question:"Αν θα μπορούσα να επιλέξω, θα προτιμούσα να μένω μόνος/η",
    subtitle:"(από το 1 έως το 5 πόσο συμφωνείς;)",
    replies:[
      { key:1, points:[1,0,0,0], content:"" },
      { key:2, points:[0,1,0,0], content:"" },
      { key:3, points:[0,0,1,1], content:"" },
      { key:4, points:[0,0,1,1], content:"" },
      { key:5, points:[0,0,1,1], content:"" },
    ],
    type:'enum'
  },
  //Question 18
  {
    question:"Ποια από τις παρακάτω θα μπορούσε να είναι η δουλειά των ονείρων σου;",
    replies:[
      { key:1, points:[0,0,1,0], content:"Καλλιτέχνης" },
      { key:2, points:[1,0,0,0], content:"Στέλεχος σε εταιρίας" },
      { key:3, points:[0,1,0,0], content:"Freelancer" },
      { key:4, points:[0,0,0,1], content:"Εκπαιδευτικός" },
      { key:5, points:[1,0,0,0], content:"Γιατρός" },
      { key:6, points:[1,0,0,0], content:"Δικηγόρος" },
    ],
    type:'regular'
  },
];
