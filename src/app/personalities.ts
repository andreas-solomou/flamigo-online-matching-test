/////////////// ENGLISH VERSION ///////////////
export const PersonalitiesEn = [
  {
    title: 'Lone Wolf',
    img: './assets/personalities/lone-wolf.png',
    descr: `The professional.

    Your discretion even when you're in one of the shared rooms of the house, reaches such levels that would make your flatmate to wonder whether you're still around at all.
    You're a trustworthy, punctual and respectful person and you desire the same treatment from the others around you.
    You like spending time with yourself or having some laid-back moments with your close friends.

    You flatshare mostly for practical reasons, more so than for the experience of it. If neither you nor your flatmates doesn't "cross the lines" of each other, you can have an absolutely harmonious cohabitation based on trust, no more no less.`,
  },
  {
    title: 'Busy Bee',
    img: './assets/personalities/busy-bee.png',
    descr: `Sociable, hardworking and energetic.

    You occupy yourself with a ton of activities and spend most of your time away from home.
    You get back only to simmer down and re-fill your batteries (figuratively and literally), to get ready for another day of excitement.

    A hot bath and a relaxing sleeping session is all you're asking for when you get back home.`,
  },
  {
    title: 'Chill Sloth',
    img: './assets/personalities/chill-sloth.png',
    descr: `The perfect day for you begings in the afternoon and it follows-up with a couple hours (or more) of chilling on your comfy couch, with takeaway food on your lap and your favorite show on the TV.

    Your love for staying at home has no end and you're looking for a person who'd be into that too.

    You're the easy-going and carefree kind of person, who's alright with spending time with your flatmates, as long as it doesn't involve physical exercise and signing off the internet.`,
  },
  {
    title: 'Cuddly Bear',
    img: './assets/personalities/cuddly-bear.png',
    descr: `Charismatic and sociable. The kind of person who always knows how to "break the ice".

    It's very important for you to maintain a friendly relationship with your flatmates 'cause, even though you may be flatsharing out of necessity, you like having someone around and you attempt to share experiences with them with every chance you get.`,
  },
];

/////////////// GREEK VERSION ///////////////
export const PersonalitiesGr = [
  {
    title: 'Lone Wolf',
    img: './assets/personalities/lone-wolf.png',
    descr: `The professional.

    Η διακριτικότητά σου ακόμα και όταν βρίσκεσαι στους κοινόχρηστους χώρος του σπιτιού, κάνει τον συγκάτοικο σου να αναρωτιέται αν βρίσκεσαι όντως στο σπίτι.
    Είσαι ένα άτομο έμπιστο και σε χαρακτηρίζουν η συνέπεια και ο σεβασμός, αξίες τις οποίες απαιτείς και να επιδεικνύουν και οι γύρω σου.
    Απολαμβάνεις να περνάς χρόνο με τον εαυτό σου ή σε χαλαρά σκηνικά με τους στενούς σου φίλους.

    Συγκατοικείς περισσότερο για πρακτικούς λόγους, παρά για την εμπειρία και εφόσον, κανένας σας δεν ξεπερνά τα όρια του άλλου, μπορείτε να έχετε μία απόλυτα αρμονική συμβίωση και να χτίσετε μία σχέση εμπιστοσύνης.`,
  },
  {
    title: 'Busy Bee',
    img: './assets/personalities/busy-bee.png',
    descr: `Δραστήριος, κοινωνικός και εργατικός.

    Ασχολείσε με μία πληθώρα δραστηριοτήτων και περνάς το μεγαλύτερο μέρος του χρόνου σου εκτός σπιτιού.
    Επιστρέφεις μόνο για να αποφορτιστείς, να  ανακτήσεις δυνάμεις και να ετοιμαστείς για την επόμενη ημέρα που θα αδράξεις.

    Ένα ζεστό ντους και ένας χαλαρωτικός ύπνος είναι το μόνο που ζητάς όταν γυρίζεις σπίτι.`,
  },
  {
    title: 'Chill Sloth',
    img: './assets/personalities/chill-sloth.png',
    descr: `Αν η ιδανική σου μέρα περιλαμβάνει “απογευματινό” ξύπνημα και χαλάρωμα στον αγαπημένο σου καναπέ με delivery και την αγαπημένη σου σειρά στη τηλεόραση, είσαι ένας Chill Sloth.

    Απολαμβάνεις χωρίς ενοχές όλες τις αμαρτωλές απολαύσεις που μπορεί να σου προσφέρει η θαλπωρή του σπιτιού σου και ψάχνεις ένα άτομο που σε αντίθεση με τη μάνα σου, θα επικροτεί τον τρόπο ζωής σου.

    Καλόβολος και ξέγνοιαστος συγκάτοικος, δέχεσαι ότι κι αν κάνει ο συγκάτοικος σου, αρκεί να μη περιλαμβάνει σωματική άσκηση και αποσύνδεση από το διαδίκτυο.`,
  },
  {
    title: 'Cuddly Bear',
    img: './assets/personalities/cuddly-bear.png',
    descr: `Χαρισματικά κοινωνικός, έχεις πάντα μια ατάκα για να σπάσει ο πάγος.

    Είναι σημαντικό για ‘σένα να διατηρείς φιλικές σχέσεις με τον συγκάτοικό σου γιατί αν και μπορεί να συγκατοικείς από ανάγκη, θες να ευχαριστιέσαι την παρέα στο σπίτι και επιδιώκεις να μοιράζεσαι εμπειρίες με τον συγκάτοικό σου.`,
  },
];
