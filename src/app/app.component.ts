import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Codes } from './codes';
import { QuestionsGr, QuestionsEn } from './questions';
import { PersonalitiesGr, PersonalitiesEn } from './personalities';


export interface Results {
  qIndex:number,
  key:number,
  points:number[]
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
// ACCESS CODE SCREEN
  codeScreen = false; //make true before launch
  codeError = false;
  matchingTestScreen = true; //make false before launch

  title = 'app';

  codes = Codes;

  theCode = new FormControl('');

  inputted(){
    if(this.codes.filter(res => res === this.theCode.value).length === 1) {
      this.codeScreen = false;
      this.matchingTestScreen = true;
    }
    else {
      this.codeError = true;
    }
  }
// MATCHING TEST
  questions = QuestionsEn;
  questionIndex: number;

  personalities = PersonalitiesEn;

  results: Results[] = [{ qIndex:null, key:null, points:[0,0,0,0] }];
  points: number[];
  vector: number[];

  progress:number = 0;
  endTest:boolean = false;

  ngOnInit() {
    this.reset();
    this.updateProgressBar();
  }

  replyClicked(key, points){
    this.results[this.questionIndex].qIndex = this.questionIndex;
    this.results[this.questionIndex].key = key;
    this.results[this.questionIndex].points = points;
    //console.log('Selected reply for \nquestion: '+this.results[this.questionIndex].qIndex+'\nhas key: '+ this.results[this.questionIndex].key +'\nand points: '+ this.results[this.questionIndex].points);
  }

  pressPrev(){
    this.questionIndex--;
    this.updateProgressBar();
  }

  pressNext(){
    this.questionIndex++;
    this.updateProgressBar();
  }

  persArr:number;

  pressEnd(){
    this.endTest = true;
    //console.log('Results');
    this.countPoints();
    this.shapeVector();
    //console.log('pers index: '+this.findPersonalities());
    this.persArr = this.findPersonalities();
    //this.persArr = [...this.persArr, ...this.findPersonalities()];
    //console.log('pers index: '+this.persArr);
  }

  updateProgressBar(){
    this.progress = 100*this.questionIndex/(this.questions.length*2)+50/(this.questions.length*2);
  }

  reset(){
    this.questionIndex = 0;
    for(let i=0; i<this.questions.length; i++){
      this.results[i] = { qIndex:null, key:null, points:[0,0,0,0] };
    }
    this.endTest = false;
    this.points = [0,0,0,0];
    this.vector = [];
    this.updateProgressBar();
  }

  countPoints(){
    for(let i=0; i<this.results.length; i++){
      for(let j=0; j<this.points.length; j++){
        this.points[j] += this.results[i].points[j];
      }
    }
    //console.log('points: '+this.points);
  }
  shapeVector(){
    for(let i=0; i<this.results.length; i++){
      this.vector[i] = this.results[i].key;
    }
    //console.log('vector: '+this.vector);
  }

  findPersonalities(){
    let max:number = 0;
    let maxIndex:number = 0;

    for(let i=0; i<this.points.length; i++){
      if(this.points[i]>max){
        max = this.points[i];
        maxIndex = i;
      }
    }
    //console.log('Verdict');
    //console.log('VERDICT\nmax: '+max+'\nmax index: '+maxIndex);
    /*
    let pers = new Array();

    for(let i=0; i<this.points.length; i++){
      if(max==this.points[i]){
        pers = [...pers, i];
      }
    }
    console.log('PERSONALITIES\n'+pers);
    return pers;
    */
    return maxIndex;
  }

// TRANSLATION
  btns_gr = { previous: 'Προηγούμενο', next: 'Επόμενο', end: 'Τέλος' };
  btns_en = { previous: 'Previous', next: 'Next', end: 'See Result'};
  btns = { previous: 'Previous', next: 'Next', end: 'See Result'};

  openTranslation:boolean = false;
  translationBtn(){
    this.openTranslation = !this.openTranslation;
  }
  changeLang(arg){
    switch(arg){
      case 'gr':{
        this.questions = QuestionsGr;
        this.personalities = PersonalitiesGr;
        this.btns = this.btns_gr;
        this.openTranslation = false;
        break;
      }
      case 'en':{
        this.questions = QuestionsEn;
        this.personalities = PersonalitiesEn;
        this.btns = this.btns_en;
        this.openTranslation = false;
        break;
      }
    }
  }
}
